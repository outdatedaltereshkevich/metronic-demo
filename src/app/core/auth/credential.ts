export interface Credential {
	username: string;
	rememberMe: boolean;
	password: string;
}
