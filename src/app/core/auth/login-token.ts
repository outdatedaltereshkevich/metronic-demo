export interface LoginToken {
	id_token: string;
	refreshToken: string;
	roles: any;
}
